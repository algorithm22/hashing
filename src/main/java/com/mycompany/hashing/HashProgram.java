/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hashing;

import java.util.Hashtable;
import java.util.Iterator;

/**
 *
 * @author Windows10
 */
public class HashProgram <Key,Value>{
        private int M = 100;
	private Value[] vals = (Value[]) new Object[M];
	private Key[] keys = (Key[]) new Object[M];

	private int hash(Key key) {
		return (key.hashCode() & 0X7FFFFFFF) % M;
	}

	public void put(Key key, Value val) {
		int i;
		for (i = hash(key); keys[i] != null; i = (i + 1) % M)
			if (keys[i].equals(key)){
                            keys[i] = key;
                            vals[i] = val;
                            break;
                        }
		keys[i] = key;
		vals[i] = val;
	}

	public Value get(Key key) {
		for (int i = hash(key); keys[i] != null; i = (i + i) % M)
			if (key.equals(keys[i]))
				return vals[i];
		return null;
	}
        
        public int remove(Key key){
            for(int i=hash(key);keys[i]!=null;i=(i+i)%M){
                if(keys[i].equals(key)){
                    int temp=(int)keys[i];
                    keys[i]=null;
                    return temp;
                }
            }
            return 0;
        }
        
        
}

