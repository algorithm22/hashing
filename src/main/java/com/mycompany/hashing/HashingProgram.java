/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hashing;


/**
 *
 * @author Windows10
 */
public class HashingProgram {

    public static void main(String[] args) {
        HashProgram<Integer,String> st = new HashProgram<Integer,String>();
        st.put(1,"Satun");
        st.put(2,"Phuket");
        st.put(3,"Phang nga");
        st.put(4,"Na khon");
        st.put(5,"Hat Yai");
        
//        System.out.println(st.get(1));
//        System.out.println(st.get(2));
//        System.out.println(st.get(3));
//        System.out.println(st.get(4));
        st.remove(2);
        
        for(int i=1;i<=5;i++){
            System.out.println(st.get(i));
        }
        
    }
}
